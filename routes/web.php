<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/products', 'ProduktController@all_products');

Route::get('/getName/{product}', 'ProduktController@only_product');

Route::get('/addProduct', 'ProduktController@add_product');

Route::post('/storeProduct', 'ProduktController@store_product');

Route::get('/findProduct', 'ProduktController@find_product');

Route::post('/searchProduct', 'ProduktController@search_product');

Route::get('/deleteProduct', 'ProduktController@delete_product');

Route::post('/eraseProduct', 'ProduktController@erase_product');

Route::get('/updateProduct/{product}', 'ProduktController@update_product');

Route::post('/actualizeProduct/{product}', 'ProduktController@actualize_product');

Route::get('getName/json/{product}', 'ProduktController@json_view');
