<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produkt;
use Illuminate\Support\Facades\Response;

class ProduktController extends Controller
{
	public function all_products()
	{
		$products = Produkt::latest()->get();
		return view('products.show_all_products', compact('products'));
	} 


    public function only_product($product_id)
    {
		$product = Produkt::find($product_id);

        if(!$product)
        {
            return Response::json('Product with this ID doesn´t exist.', 404);
        }
        else 
        {
            return view('products.show', compact('product'));
        }
    }


    public function add_product()
    {
    	return view('products.add');
    }


    public function store_product()
    {
    	$this->validate(request(), [
    		'name' => 'required'
    	]);

    	Produkt::create(request(['name']));

    	return redirect('/products');
    }


    public function find_product()
    {
    	return view('products.find');
    }


    public function search_product()
    {
    	$this->validate(request(), [
    		'id' => 'required'
    	]);

    	$product = Produkt::find(request('id'));

    	if(!$product)
    	{
            return Response::json('Product with this ID doesn´t exist.', 404);
    	}
    	else 
    	{
    		return view('products.show', compact('product'));
    	}
    	
    }


    public function delete_product()
    {
    	return view('products.delete');
    }


    public function erase_product()
    {
    	$this->validate(request(), [
    		'id' => 'required'
    	]);

    	$produkt = Produkt::find(request('id'));

    	if($produkt)
    	{
    		$produkt->delete();
    	}
    	else
    	{
    		$hlaska = 'Product with this ID doesnt exist';
    		dd($hlaska);
    	}

    	return redirect('/products');
    }


    public function update_product(Produkt $product)
    {
    	return view('products.update', compact('product'));
    }


    public function actualize_product(Produkt $product)
    {
    	$this->validate(request(), [
    		'name' => 'required'
    	]);

    	$product->name = request('name');
    	$product->save();

    	return redirect("/getName/{$product->id}");
    }

    public function json_view(Produkt $product)
    {
        return Response::json($product->name, 200);
    }
}
