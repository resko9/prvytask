@extends ('layouts.master')

@section('content')

	<h2> Here you can update the product name of product <i><ins>{{ $product->name }}</ins></i> </h2>
	<hr>
	<form method="POST" action="/actualizeProduct/{{ $product->id }}">

		  {{ csrf_field() }}	

		  <div class="form-group">
		    <label for="name">Name:</label>
		    <input type="body" class="form-control" id="name" name="name">
		  </div>

		  <div class="form-group">
			  <button type="submit" class="btn btn-default">Save</button>
		  </div>	  

		  @include ('layouts.errors')

		</form>

@endsection('content')