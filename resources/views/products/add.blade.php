@extends ('layouts.master')

@section ('content')

		<h1>Add a Product</h1>

		<hr>

		<form method="POST" action="/storeProduct">

		  {{ csrf_field() }}	

		  <div class="form-group">
		    <label for="name">Name:</label>
		    <input type="body" class="form-control" id="name" name="name">
		  </div>

		  <div class="form-group">
			  <button type="submit" class="btn btn-default">Save</button>
		  </div>	  

		  @include ('layouts.errors')

		</form>

@endsection