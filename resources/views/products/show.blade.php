@extends ('layouts.master')

@section('content')

	<h1>produkt ID {{ $product->id }}:</h1>
	<h3>{{ $product->name }}</h3>

	<h3><a href='/getName/json/{{ $product->id }}'>Show JSON version</a></h3>

	<input type="button" value="Return to Main page" onclick="window.location.href='/'">
	<input type="button" value="Return to all products" onclick="window.location.href='/products'">
	<input type="button" value="Update this product" onclick="window.location.href='/updateProduct/{{ $product->id }}'">

@endsection