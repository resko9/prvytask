@extends ('layouts.master')

@section ('content')

		<h1>Delete Product</h1>

		<hr>

		<form method="POST" action="/eraseProduct">

		  {{ csrf_field() }}	

		  <div class="form-group">
		    <label for="name">ID:</label>
		    <input type="number" class="form-control" id="id" name="id" required>
		  </div><br>

		  <div class="form-group">
			  <button type="submit" class="btn btn-default">Delete</button>
		  </div>	  

		 @include ('layouts.errors')

		</form>

		<hr>

		<input type="button" value="Return to Main page" onclick="window.location.href='/'">

@endsection