@extends ('layouts.master')

@section('content')

    <h1>This is list of all our products:</h1>
    <ul>
        @foreach ($products as $products)
            <li>
            	<a href="/getName/{{ $products->id }}"> 
            		{{ $products->name }} 
                </a>
                <br>
	            created: {{ $products->created_at->diffForHumans() }}
            </li> <br>
        @endforeach
    </ul>

    <input type="button" value="Return to Main page" onclick="window.location.href='/'">

@endsection