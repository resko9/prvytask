<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login page</title>
    </head>
    <body>
            @if (Route::has('login'))
                <div class="login links">
                    @auth
                        <h3><a href="{{ url('/home') }}">My profile</a></h3>
                        <h3><a href="{{ url('/products') }}">Products</a></h3>
                        <h3><a href="{{ url('/addProduct') }}">Add product</a></h3>
                        <h3><a href="{{ url('/findProduct') }}">Find product</a></h3>
                        <h3><a href="{{ url('/deleteProduct') }}">Delete product</a></h3>
                    @else
                        <h2 style="text-align:center;">Welcome on this page. You have to either Login or Register.</h2>
                        <h4><a href="{{ route('login') }}">Login</a></h4>
                        <h4><a href="{{ route('register') }}">Register</a></h4>
                    @endauth
                </div>
            @endif
    </body>
</html>
